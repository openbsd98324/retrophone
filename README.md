# retrophone


# Retrophone aka. the PiPhone to make VoIP Phone Calls over Internet 




## Introduction 


The PiPhone is a project to restore standard telephony. The raspberry pi is connected to internet over the wireless using board wlan device.

This uses the following hardware mostly: PiZero, VOIP USB Phone device, SD-card, 
and power supply. 

The total cost is below 50 Euros. 

![](img/scr_pub_retrophone_01.jpg)

![](https://gitlab.com/openbsd98324/retrophone/-/raw/master/img/20210216180919F1-20210216180539.jpg)


## Installation

The device is using the RaspiOS Buster or Devuan Ascii.
The software Linphone allows to make a phone call. 
The VOIP USB has integrated ringer. 

Linphone over "Xvfb :1 -screen 0 800x600x16" or linphonec with tmux can be set to run in background. 




## OS

wget -c --no-check-certificate   "https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-05-28/2021-05-07-raspios-buster-armhf-lite.zip"

apt-get update

apt-get install linphone-nogtk 




## Conclusion

It requires less than 50 Euros to make good quality VOIP calls over the internet.

*Benefits:* no need of any account like Apple, Google, Whatapps, ... that may use your personal data.




